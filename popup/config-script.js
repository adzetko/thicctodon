console.log("now listening for clicks")
let currentColumnWidth = ""
function removePrevious(previousColumnWidth) {
  if (currentColumnWidth == "") return

  return browser.tabs
    .removeCSS({ code: `body > #mastodon .columns-area > .column {width: ${previousColumnWidth};}` })

}

document.addEventListener("click", (e) => {

  console.log("button clicked")
  removePrevious(currentColumnWidth)

  switch (e.target.textContent) {
    
    case "thin":
      console.log("it was thin")
      currentColumnWidth = "250px"
      break
    
    case "regular":
      console.log("it was regular")
      currentColumnWidth = "350px"
      break
    
    case "thicc":
      console.log("it was thicc")
      currentColumnWidth = "500px"
      break
  }
  
  return browser.tabs
    .insertCSS({ code: `body > #mastodon .columns-area > .column {width: ${currentColumnWidth};}` })
})
